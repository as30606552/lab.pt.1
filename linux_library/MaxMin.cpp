#include "MaxMin.h"

int Max(const int v[], int size) {
	int max = v[0];
	for (unsigned int i = 1; i < size; i++) {
		if (max < v[i])
			max = v[i];
	}
	return max;
} 

int Min(const int v[], int size) {
	int min = v[0];
	for (unsigned int i = 1; i < size; i++) {
		if (min > v[i])
			min = v[i];
	}

	return min;
}

