#!/bin/bash
echo Creating the libmaxmin.so library...
g++ -Wall -shared MaxMin.cpp -o libmaxmin.so
sudo mv libmaxmin.so /usr/local/lib
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
export LD_LIBRARY_PATH
echo Done
