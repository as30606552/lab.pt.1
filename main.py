import ctypes
import sys
import platform
import random


def is_int(value):
    try:
        int(value)
        return True
    except ValueError:
        return False


INT_CAST_ERROR = "Ошибка ввода (должно быть целым числом)!"


def main(args):
    print("Программа для вычисления минимального и максимального элементов массива")
    p = platform.system()
    print(f"Операционная система: {p}")
    if p != "Windows" and p != "Linux":
        print("К сожалению, ваша ОС не поддерживается")
        sys.exit(1)
    n = input("Введите длинну массива: ")
    if not is_int(n):
        print(INT_CAST_ERROR)
        sys.exit(1)
    n = int(n)
    v = (ctypes.c_int * n)()
    is_random = input("Заполнить массив произвольными числами? (y/n): ")
    if is_random.lower() == "y":
        for i in range(n):
            v[i] = int(random.random() * 100)
        print("Сгенерирован массив: [", end="")
        for i in range(n - 1):
            print(v[i], end=", ")
        print(v[n - 1], end="]\n")
    else:
        for i in range(n):
            tmp = input(f"Введите элемент массива a[{i}]: ")
            if not is_int(tmp):
                print(INT_CAST_ERROR)
                sys.exit(1)
            tmp = int(tmp)
            v[i] = tmp
    if p == "Windows":
        print("Подгружаю библиотеку MaxMin.dll...", end="")
        lib = ctypes.CDLL("windows_library/MaxMin.dll")
        print("Готово")
    elif p == 'Linux':
        print("Подгружаю библиотеку libmaxmin.so...", end="")
        lib = ctypes.CDLL("libmaxmin.so")
        print("Готово")
    print("Максимум:", lib.Max(v, n))
    print("Минимум:", lib.Min(v, n))


if __name__ == "__main__":
    main(sys.argv)
