#pragma once

#ifdef LIB_EXPORT
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif

extern "C" LIB_API int Max(int numbers[], int size);
extern "C" LIB_API int Min(int numbers[], int size);